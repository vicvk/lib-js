
/*

<script src="{{ asset('assets-vicvk/uploader/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('assets-vicvk/uploader/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('assets-vicvk/uploader/jquery.fileupload.js') }}"></script>


!! input MUST have an id. This id is used to accumulate uploaded file names.

<input id="fileupload" type="file" name="files[]" data-url="/uploader/" multiple>


mylib.initFileUploader('#fileupload', {

    maxNumberOfFiles : 6,  // not necessary if <input type="file"> doesn't have a multiple property

    onUploadCompleted: function (uploadedFiles) {

    },

    onProgressAll: function (progressPercents) {

    },

});


*/

mylib.fileUploaderFiles = {};

mylib.initFileUploader = function(inputFileSelector, options)
{
//    alert('inited');

    if (options.hasOwnProperty('maxNumberOfFiles'))
    {
        var maxNumberOfFiles = options.maxNumberOfFiles;
    }
    else
    {
        var maxNumberOfFiles = 1;
    }

    $(inputFileSelector).each(function( index ) 
    {
        var currentInputFileId = $( this ).attr('id');

        if (!currentInputFileId)
        {
            alert('Error in mylib.initFileUploader() - every input of file type must have an id attribute');
            return;
        }
//        console.log(  );


///        alert( $('#' + currentInputFileId).data('url') );


        $('#' + currentInputFileId).fileupload(
        {
            dataType: 'json',

            maxChunkSize: 1000000, // 1 MB
            maxRetries: 100,
            retryTimeout: 500,

            fail: function (e, data) {
                    // jQuery Widget Factory uses "namespace-widgetname" since version 1.10.0:
                    var fu = $(this).data('blueimp-fileupload') || $(this).data('fileupload'),
                    retries = data.context.data('retries') || 0,
                    retry = function () {

                        var uploaderUrl = $('#' + currentInputFileId).data('url');

                        $.getJSON(uploaderUrl, {file: data.files[0].name})
                            .done(function (result) {
                                var file = result.file;
                                data.uploadedBytes = file && file.size;
                                // clear the previous data:
                                data.data = null;
                                data.submit();
                            })
                            .fail(function () {
                                fu._trigger('fail', e, data);
                            });
                    };
                if (data.errorThrown !== 'abort' &&
                        data.uploadedBytes < data.files[0].size &&
                        retries < fu.options.maxRetries) {
                    retries += 1;
                    data.context.data('retries', retries);
                    window.setTimeout(retry, retries * fu.options.retryTimeout);
                    return;
                }
                data.context.removeData('retries');
                $.blueimp.fileupload.prototype
                    .options.fail.call(this, e, data);
            },


            //maxNumberOfFiles: 6,  <- it doesn't work

            add: function (e, data) {

                //$('div#progress').text('0');

                //console.log('Uploads started');

                mylib.fileUploaderFiles[currentInputFileId] = [];

                //fileUploaderFiles = [];

                data.submit();
            },

            change : function (e, data) {
            // maybe change => submit
            // https://stackoverflow.com/questions/16011200/jquery-file-upload-restricting-number-of-files

                if (data.files.length > maxNumberOfFiles) {
                    alert("Max " + maxNumberOfFiles + " files are allowed")
                    return false;
                }
            },

            done: function (e, data) 
            {
                //console.log(data.result);
                //console.log(data.result.files);

                var filesCount = data.result.files.length;

                for (var i=0; i<filesCount; i++)
                {
                    //console.log('#');
                    //console.log(data.result.files[i].name);

                    mylib.fileUploaderFiles[currentInputFileId].push(data.result.files[i].name);

                    //fileUploaderFiles.push(data.result.files[i].name);
                }

                //console.log('---------------------');


                //$.each(data.result.files, function (index, file) {
                //    $('<p/>').text(file.name).appendTo(document.body);
                //});

            },

            stop: function (e) 
            {
                //console.log('Uploads finished');
                //console.log(fileUploaderFiles);

                options.onUploadCompleted(mylib.fileUploaderFiles[currentInputFileId]);

                //uploadCompleted(fileUploaderFiles);
            },

            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                options.onProgressAll(progress);

                //$('div#progress').text(progress);

                //$('#progress .bar').css(
                //    'width',
                //    progress + '%'
                //);
            }


        });


    });


}

