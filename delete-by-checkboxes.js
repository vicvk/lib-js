/*
Send HTTP delete request to a given URL, with optional confirmation dialog.

<a href="#" class="mylib-delete-by-checkboxes-command" data-delete-url="{{ route('products.index') }}" data-jquery-selector-for-checkboxes=".mylib-select-checkboxes" data-confirm="Are you sure to delete selected item(s)?" data-nothing-selected="Please select one or more items">Delete Selected</a>

class="mylib-delete-by-checkboxes-command" is a required class attribute. Delete by checked checkboxes links are selected by this class.
data-delete-url is a required attribute.
data-jquery-selector-for-checkboxes is not requred. Default value is ".mylib-select-checkboxes".
data-confirm is not required. Default value is "Are you sure to delete selected item(s)?".
If this value equals an empty string, no confirmation will be asked.

data-nothing-selected is not required. This is a warning message that will be displayed to user
in case if no items were selected. Defaults to: "Please select one or more items"

Minimal usage with all default values:
<a href="#" class="mylib-delete-by-checkboxes-command" data-delete-url="{{ route('products.index') }}">Delete Selected</a>

Minimal usage with all default values and styled as bootstrap button:
<a href="#" class="btn btn-danger mylib-delete-by-checkboxes-command" role="button" data-delete-url="{{ route('products.index') }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete</a>

*/                                                                                                                                                                             

(function() {
 
    var myVar = {
        initialize: function() {
            this.methodLinks = $('.mylib-delete-by-checkboxes-command');
            // ?? need to remove previously attached events. Will subsequent calls to initialize
            // attach multiple events?
            this.methodLinks.off('click');
            this.methodLinks.on('click', this.handleMethod);

            this.toggleCheckboxes = $('.mylib-toggle-checkboxes');
            // ?? need to remove previously attached events. Will subsequent calls to initialize
            // attach multiple events?
            this.toggleCheckboxes.off('click');
            this.toggleCheckboxes.on('click', this.handleToggleCheckboxes);
        },
     
        handleMethod: function(e) {
            e.preventDefault();

            var link = $(this);
            var deleteUrl = link.data('delete-url');
            var jquerySelectorForCheckboxes = link.data('jquery-selector-for-checkboxes');
            var confirm = link.data('confirm');
            var nothing_selected = link.data('nothing-selected');

            if (jquerySelectorForCheckboxes === undefined)
            {
                jquerySelectorForCheckboxes = '.mylib-select-checkboxes';
            }

            if (confirm === undefined)
            {
                confirm = 'Are you sure to delete selected item(s)?';
            }

            if (nothing_selected === undefined)
            {
                nothing_selected = 'Please select one or more items';
            }

            var idsArray = $(jquerySelectorForCheckboxes+":checked").map(function() {
                return this.value;
            }).get();

//console.log('here');
//console.log(idsArray);


            if (idsArray.length == 0)
            {
                mylib.popupWarning(nothing_selected);
                return;
            }

            if (confirm != "")
            {
                if (!mylib.confirm(confirm))
                {
                    return false;
                }
            }


            var onSuccess = function(data, textStatus, jqXhr) {
////                alert('deleted');

                // the intention is to reload the page without resubmitting any POST data (in case if there 
                // was any previous POST request). And also we want to clear any such POST request from
                // browser history, that is why a "replace" here.
                window.location.replace(window.location.href);

                /// maybe?
                ///window.location.href = window.location.href;
            }

//console.log('to-delete');

            mylib.deleteByIds(deleteUrl, idsArray, onSuccess);

        },
   
        handleToggleCheckboxes: function(e) {

            var chk = $(this);

            if (chk.is('a')) {
                e.preventDefault();
            }

            var jquerySelectorForCheckboxes = chk.data('jquery-selector-for-checkboxes');

            if (jquerySelectorForCheckboxes === undefined) {
                jquerySelectorForCheckboxes = '.mylib-select-checkboxes';
            }

            var newCheckedStatus = true;

            if (chk.is(':checkbox')) {
                
                if (this.checked) {
                    newCheckedStatus = true;
                }
                else {
                    newCheckedStatus = false;
                }
            }
            else {
                // if it's not a checkbox, then newCheckedStatus will be determined
                // by the first element in the group

                var first = $(jquerySelectorForCheckboxes).first();

                if (first) {
                    if (first.is(':checked')) {
                        newCheckedStatus = false;
                    }
                    else {
                        newCheckedStatus = true;
                    }
                }

                // Also it's good to assign the newCheckedStatus to all appropriate
                // "Select/Unselect All" control checkboxes
                $('input[type=checkbox].mylib-toggle-checkboxes').each(function() {

////alert($(this).data('jquery-selector-for-checkboxes'));

                    var thisJquerySelectorForCheckboxes = $(this).data('jquery-selector-for-checkboxes');

                    if (thisJquerySelectorForCheckboxes === undefined) {
                        thisJquerySelectorForCheckboxes = '.mylib-select-checkboxes';
                    }

                    if (thisJquerySelectorForCheckboxes == jquerySelectorForCheckboxes) {
                        this.checked = newCheckedStatus;
                    }
                });
            }


            $(jquerySelectorForCheckboxes).each(function() {
                this.checked = newCheckedStatus;
            });


            chk.blur(); // remove focus from checkbox
        },


    };
   
    myVar.initialize();
 
})();


