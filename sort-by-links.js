(function() {
 
    var myVar = {
        initialize: function() {
            this.methodLinks = $('.sorting').add('.sorting_asc').add('.sorting_desc');
            // ?? need to remove previously attached events. Will subsequent calls to initialize
            // attach multiple events?
            this.methodLinks.off('click');
            this.methodLinks.on('click', this.handleMethod);

            // Now for those links which mode=fragment we should set current sorting state class

            var queryStr = window.location.hash.substring(1);
            var queryObj = mylib.parseQueryString(queryStr);

            var currentSortBy = '';
            var currentSortOrder = '';

            if ( queryObj.hasOwnProperty('_sb') ) {
                currentSortBy = queryObj._sb;
            }

            if ( queryObj.hasOwnProperty('_so') ) {
                currentSortOrder = queryObj._so;
            }

            if ((currentSortBy != '') && (currentSortOrder != '')) {

                this.methodLinks.each(function( index ) {
                    var link = $(this);

                    if (link.data('mode') == 'fragment') {

                        link.removeClass('sorting sorting_asc sorting_desc');

                        if (link.data('tag') == currentSortBy) {

                            var href = link.attr('href');
                            href = href.substring(1); // remove the starting hash character
                            var sortQueryObj = mylib.parseQueryString(href);

                            if (currentSortOrder == 'asc') {
                                link.addClass('sorting_asc');
                                sortQueryObj._so = 'desc';
                                link.attr('href', '#'+mylib.buildSortedQueryString(sortQueryObj));
                            }
                            else if (currentSortOrder == 'desc') {
                                link.addClass('sorting_desc');
                                sortQueryObj._so = 'asc';
                                link.attr('href', '#'+mylib.buildSortedQueryString(sortQueryObj));
                            }
                            else {
                                link.addClass('sorting');
                            }
                        }
                        else {
                            link.addClass('sorting');
                        }

                    }

                });
            }
        },

        handleMethod: function(e) {
            e.preventDefault();

            var link = $(this);

            var href = link.attr('href');
            href = href.substring(1); // remove the starting hash character
            var sortQueryObj = mylib.parseQueryString(href);

            var mode = link.data('mode');

            if (mode == 'query') {
                var queryStr = window.location.search.substring(1);
                var queryObj = mylib.parseQueryString(queryStr);

                var newQueryObj = mylib.mergeObjects(queryObj, sortQueryObj);
                var newQueryStr = mylib.buildSortedQueryString(newQueryObj);

                if (newQueryStr != '') {
                    newQueryStr = '?' + newQueryStr;
                }

                var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + newQueryStr + window.location.hash;

                window.location.replace(newUrl);
            }
            else if (mode == 'fragment') {
                link.blur(); // remove focus from sort link

                var hashStr = window.location.hash.substring(1);
                var hashObj = mylib.parseQueryString(hashStr);

                var newHashObj = mylib.mergeObjects(hashObj, sortQueryObj);
                var newHashStr = mylib.buildSortedQueryString(newHashObj);

                if (newHashStr != '') {
                    newHashStr = '#' + newHashStr;
                }

                var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search + newHashStr;

                // detect new sort order and set correct css classes

                var currSortOrder, nextSortOrder;

                for (var paramName in sortQueryObj) {
                    if (sortQueryObj.hasOwnProperty(paramName)) {
                        paramNameEnding = paramName.substring(paramName.length-3);

                        if (paramNameEnding == '_so') {
                            currSortOrder = sortQueryObj[paramName];
                            break;
                        }
                    }
                }

                if (currSortOrder == 'asc') {
                    nextSortOrder = 'desc';
                }
                else {
                    nextSortOrder = 'asc';
                }

                sortQueryObj[paramName] = nextSortOrder;

                link.attr('href', '#'+mylib.buildSortedQueryString(sortQueryObj));


                // find all the neighbours of the currently clicked link, 
                // having class .sorting_asc OR .sorting_desc
                var closestCommonParent = link.parent('th, td, li').parent('tr, ul');
                var neighbours = closestCommonParent.find('.sorting_asc, .sorting_desc');

                neighbours.removeClass('sorting_asc sorting_desc');
                neighbours.addClass('sorting');

                if (link.hasClass('sorting')) {
                    link.removeClass('sorting');
                    link.addClass('sorting_'+currSortOrder);
                }
                else {
                    var link_immediate_parent = link.parent('.sorting');
                    link_immediate_parent.removeClass('sorting');
                    link_immediate_parent.addClass('sorting_'+currSortOrder);
                }

                window.location.replace(newUrl);
                mylib.loadListItems();
            }
        },
    };

    myVar.initialize();

})();
