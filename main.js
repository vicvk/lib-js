/*  
I want to namespace my functions to "mylib" name space. Like this:  mylib.csrf_token()
*/
var mylib = {

    refreshWindow: function() {
        // the intention is to reload the page without resubmitting any POST data (in case if there 
        // was any previous POST request). And also we want to clear any such POST request from
        // browser history, that is why a "replace" here.

///alert('here');

        window.location.replace(window.location.href);

        /// maybe?
        ///window.location.href = window.location.href;
    },

    csrfToken: function() {
        // Read token from a meta tag. The meta tag should be added like this:
        // <meta name="csrf-token" content="{{ csrf_token() }}">
        return $('meta[name="csrf-token"]').attr('content');

        // NYI: Another alternative is to look for a token in a cookie. Laravel
        // sets a XSRF-TOKEN for every request. We can access it like: $.cookie["XSRF-TOKEN"]
        // but in order to read cookies from javascript we need either a speaicl jquery plugin
        // or a custom function. So for now I decided to stick with using meta tag to get the 
        // token value.
    },

    parseQueryString: function(queryStr) {
        if (typeof queryStr != "string" || queryStr.length == 0) return {};

        // Remember that semicolon ; is an equivalent to & and can be used as a valid
        // separator between parameter pairs
        var vars = queryStr.split(/[\&\;]+/);
        var vars_length = vars.length;
        var pair;
        var paramName;
        var paramValue;
        var result = {};

        for (var i=0; i<vars_length; i++) {
            pair = vars[i].split("=");

            if (0 in pair) {
                // Because a plus(+) character should be decoded to space character, which
                // decodeURIComponent() function doesn't do
                paramName = decodeURIComponent((pair[0]+'').replace(/\+/g, '%20'));

                if (paramName != '') {
                    if (1 in pair) {
                        // Because a plus(+) character should be decoded to space character, which
                        // decodeURIComponent() function doesn't do
                        paramValue = decodeURIComponent((pair[1]+'').replace(/\+/g, '%20'));
                    }
                    else {
                        paramValue = undefined;
                    }

                    if (typeof result[paramName] == "undefined") result[paramName] = paramValue;
                    else if (result[paramName] instanceof Array) result[paramName].push(paramValue);
                    else result[paramName] = [result[paramName], paramValue];
                }
            }
        }

        return(result);
    },

    //
    // From here: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
    // To be more stringent in adhering to RFC 3986 (which reserves !, ', (, ), and *), even though 
    // these characters have no formalized URI delimiting uses, the following can be safely used:
    //
    fixedEncodeURIComponent: function(str) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
          return '%' + c.charCodeAt(0).toString(16);
        });
    },

    // 
    // When is a space in a URL encoded to +, and when is it encoded to %20?
    // So basically: Target of GET submission is 
    // http://www.bing.com/search?q=hello+world
    // and a resource with space in the name 
    // http://camera.phor.net/cameralife/folders/2012/2012-06%20Pool%20party/
    // In other words: You should have %20 before the ? and + after.
    // 
    // Also from here: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
    // For application/x-www-form-urlencoded, spaces are to be replaced by '+', so one may wish to follow a encodeURIComponent replacement with an additional replacement of "%20" with "+".
    // 
    fixedEncodeURIComponentWithSpaces: function(str) {

///        return encodeURIComponent(str).replace(new RegExp('%20', 'g'), '+');

        return mylib.fixedEncodeURIComponent(str).replace(new RegExp('%20', 'g'), '+');
    },


    buildSortedQueryString: function(paramsObj) {
///
///        var serializedNotSorted = jQuery.param(paramsObj);
///
///        var vars = queryStr.split('&');
///        var vars_length = vars.length;
///

        var keys = [];
        var result = [];

        var k, k2;

        for (k in paramsObj) {
            if (paramsObj.hasOwnProperty(k)) {
                keys.push(k);
            }
        }

        keys.sort();

        var len = keys.length;

        for (var i=0; i<len; i++) {
            k = keys[i];

            if (paramsObj[k] instanceof Array) {
                var kEnding = k.substring(k.length-2);

                if (kEnding != '[]') {
                    k2 = k + '[]';
                }
                else {
                    k2 = k;
                }

                var len2 = paramsObj[k].length;

                for (var i2=0; i2<len2; i2++) {
                    result.push(mylib.fixedEncodeURIComponentWithSpaces(k2) + '=' + mylib.fixedEncodeURIComponentWithSpaces(paramsObj[k][i2]));
                }
            }
            else {
                result.push(mylib.fixedEncodeURIComponentWithSpaces(k) + '=' + mylib.fixedEncodeURIComponentWithSpaces(paramsObj[k]));
            }
        }

        return result.join('&');
    },

    getAllParams: function() {
        var hashObj = mylib.parseQueryString(window.location.hash.substring(1));
        var searchObj = mylib.parseQueryString(window.location.search.substring(1));

        // searchObj params overwrite hashObj params if parame names are the same
        var params = mylib.mergeObjects(hashObj, searchObj);

        return params;
    },

    getHelperParams: function() {
        // We should look for these params:
        // _sb
        // _so
        // _pg
        // _rp
        var paramNameEnding;
        var result = {};

        var params = mylib.getAllParams();

///alert(combinedParamsStr);
///return;

///console.log(params);
///return;

        // OR! Another possible criteria to distinguish between helper and non-helper params
        // is that helper params always start with _ (underscore)
        for (var paramName in params) {
            if (params.hasOwnProperty(paramName)) {
                paramNameEnding = paramName.substring(paramName.length-3);

                if ((paramNameEnding == '_sb') || (paramNameEnding == '_so') || (paramNameEnding == '_pg') || (paramNameEnding == '_rp')) {
                    result[paramName] = params[paramName];
                }
            }
        }

        // NYI: search window.location.pathname for /page=2 at the very end of pathname

///console.log(result);

///alert(mylib.buildSortedQueryString(result));

        return(result);
    },

    /*
      properties of obj2 overwrite properties of obj1 in case if their names overlap
    */
    mergeObjects: function(obj1, obj2) {
        var result = {};
        var k;

        for (k in obj1) {
            if (obj1.hasOwnProperty(k)) {
                result[k] = obj1[k];
            }
        }

        for (k in obj2) {
            if (obj2.hasOwnProperty(k)) {
                result[k] = obj2[k];
            }
        }

        return result;
    },

    selectNonEmptyProperties: function(obj) {
        var result = {};
        var k;

        for (k in obj) {
            if (obj.hasOwnProperty(k)) {
                if ((obj[k] === undefined) || (obj[k] === null) || (obj[k] === '')) {
                }
                else {
                    result[k] = obj[k];
                }
            }
        }

        return result;
    },

    confirm: function(message) {
        return confirm(message);
    },

    // debugError is not supposed to be seen by users, it should only display errors
    // to developers and/or log them somehow
    debugError: function(message, debugData) {
        console.log(debugData);
        return alert(message);
    },

    popupError: function(message, title) {
        var text;

        if (title === undefined)
        {
            text = message;
        }
        else
        {
            text = title + ': ' + message;
        }

        return alert(text);
    },

    popupWarning: function(message, title) {
        var text;

        if (title === undefined)
        {
            text = message;
        }
        else
        {
            text = title + ': ' + message;
        }

        return alert(text);
    },

    popupSuccess: function(message, title) {
        var text;

        if (title === undefined)
        {
            text = message;
        }
        else
        {
            text = title + ': ' + message;
        }

        return alert(text);
    },

    popupInfo: function(message, title) {
        var text;

        if (title === undefined)
        {
            text = message;
        }
        else
        {
            text = title + ': ' + message;
        }

        return alert(text);
    },

    /*
    Convert errors object which has this structure to an unordered HTML list string:

    var errorsObject = {
        'name': ['name is invalid', 'name is too short'],
        'email': ['email is required']
    }

    <ul>
    <li>name is invalid</li>
    <li>name is too short</li>
    <li>email is required</li>
    </ul>

    */
    errorsObjectToHtmlList: function(errorsObject) {
        var errorsHtml = '';

        $.each(errorsObject, function(inputName, inputMessages) {

            $.each(inputMessages, function(messageIdx, messageStr) {
                errorsHtml += '<li>' + messageStr + '</li>'; 
            });

        });

        if (errorsHtml != '') {
            errorsHtml = '<ul>' + errorsHtml + '</ul>';
        }

        return errorsHtml;
    },

    clearFormErrors: function(formObj, errorsObject) {

        // Clear common errors div

        var formId = formObj.attr('id');

        if (formId === undefined) {
            formId = '';
        }

        $('div.mylib-errors-for-form-' + formId).hide();

        $('div.mylib-errors-for-form-' + formId + ' ul').html('');


        // Clear error messages for every div.form-group

        var formGroups = formObj.find('div.form-group');

        formGroups.each(function( index ) {

            $(this).removeClass('has-error');

            // remove my help blocks
            var my_help_blocks = $(this).find('span.mylib-form-input-error-help-block');
            my_help_blocks.remove();


            // show back other help blocks
            var other_help_blocks = $(this).find('span.mylib-temporarily-hidden-help-block');
            other_help_blocks.show();
            other_help_blocks.removeClass('mylib-temporarily-hidden-help-block');
        });
    },


    displayFormErrors: function(formObj, errorsObject) {

//alert('displayFormErrors');
////console.log( errorsObject );

        var errorsHtml = '';

        var formGroups = formObj.find('div.form-group');

        $.each(errorsObject, function(inputName, inputMessages) {
//alert(inputName);

            var errorsForInputNameHasBeenDisplayedInline = false;

            formGroups.each(function( index ) {
                // $(this) is a div.form-group

                var inputs = $(this).find(':input');

                if (inputs.length > 0) {
                    // div.form-group is supposed to only contain one input
                    var input = inputs.first();

                    if (input.attr('name') == inputName) {
                        $(this).addClass('has-error');

                        var other_help_blocks = $(this).find('span.help-block');
                        other_help_blocks.addClass('mylib-temporarily-hidden-help-block');
                        other_help_blocks.hide();

                        input.after('<span class="help-block mylib-form-input-error-help-block">' + inputMessages[0] + '</span>');

                        errorsForInputNameHasBeenDisplayedInline = true;
                    }
                }
            });


            if (!errorsForInputNameHasBeenDisplayedInline) {
                $.each(inputMessages, function(messageIdx, messageStr) {
                    errorsHtml += '<li>' + messageStr + '</li>'; 
                });
            }


        });


        var formId = formObj.attr('id');

        if (formId === undefined) {
            formId = '';
        }

        if (errorsHtml == '') {

            // if all errors has been displayed inline and the red div with errors is not shown,
            // then we must attract user's attention by displaying a popop message
            var popupTitle = $('div.mylib-errors-for-form-' + formId + ' span').html();
            mylib.popupError(popupTitle);
        }
        else {

            $('div.mylib-errors-for-form-' + formId + ' ul').html(errorsHtml);
            $('div.mylib-errors-for-form-' + formId).show();

////alert(errorsHtml);
        }


///alert(popupTitle);

///        mylib.popupError('1111');

    },


/*
Will send a delete request to a given URL, passing an array if ids to be deleted.
Executes onSuccess function if everything went smoothly. In case of validation
errors, displays the errors via mylib.popupError()

Example:
var url = 'http://yahoo.com';
var idsArray=[1, 7, 85, 4]
var onSuccess = function (data) {
  // do something
};

*/
    deleteByIds: function(url, idsArray, onSuccess) {

////console.log('!!!!!!');
////console.log(url);


        $.ajax({
            type    : 'POST',
            url     : url,
            data    : { 'ids[]': idsArray, '_method':'delete', '_token': mylib.csrfToken()},
            ///dataType: "json",
            success : onSuccess,

            error   : function(jqXhr, textStatus, errorThrown) {

///alert('textStatus='+textStatus);
///console.log( jqXhr );

                // errors that are supposed to be displayed to user always come in json format
                if (jqXhr.hasOwnProperty('responseJSON')) {
                    var errorsObject = jqXhr.responseJSON;

                    var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

////alert(errorsHtml);

                    mylib.popupError(errorsHtml);
                }
                else {
                    var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

                    mylib.debugError('Error in main.js => deleteByIds(): ' + errorThrown, debugData);
                }
            }
        });

    },

    onLoadListItemsSuccess: function(data, textStatus, jqXhr) {

    },

    loadListItems: function() {
//alert('loadListItems');

        // send ajax request to current url with query string
        var url = window.location.origin + window.location.pathname;

        // We should add a suffix "ajax-load-list-items" to the end of url.
        // http://derek/admin/building  ===>>  http://derek/admin/building/ajax-load-list-items
        // http://derek/admin/building/page=3/  ===>>  http://derek/admin/building/page=3/ajax-load-list-items
        // This is needed because when we make an ajax POST request to this url, we must
        // distinguish between an ajax call to loadListItems and a POST request to store
        // a new item to collection.

        if (url.charAt(url.length - 1) == '/') {
            url = url + 'ajax-load-list-items';
        }
        else {
            url = url + '/' + 'ajax-load-list-items';
        }

///alert('loadListItems: ' + url);
///alert(window.location.origin);
///alert(window.location.pathname);

        var params = mylib.getAllParams();

        params._token = mylib.csrfToken();

        $.ajax({
            // The type of ajax call must be POST because when it is GET, there is an issue with the
            // Back browser button - when you are on a search results page and click the link to
            // detail record and then click Back - the contents of the last ajax GET call is
            // displayed instead of the last page.
            type    : 'POST',
            url     : url,
            data    : params,
            dataType: "json", // this is needed so that contoller knew that we expect JSON response from it
            success : function(data, textStatus, jqXhr) {
///alert('success');

                $('table.mylib-ajax-list-container tbody, div.mylib-ajax-list-container, ul.mylib-ajax-list-container').html(data.items_html);
                $('.mylib-ajax-list-paginator-container').html(data.paginator_html);

                if (!mylib.paginationLinks) {
                    alert('mylib.paginationLinks is undefined, most likely pagination-links.js was not included');
                }

                mylib.paginationLinks.initialize();

                if (mylib.ajaxCall) {
                    mylib.ajaxCall.initialize();
                }

                if (mylib.returnBack) {
                    mylib.returnBack.initialize();
                }

                mylib.onLoadListItemsSuccess(data, textStatus, jqXhr);

// data.url
            },

            error   : function(jqXhr, textStatus, errorThrown) {

///alert('error');
///
///alert('textStatus='+textStatus);
///console.log( jqXhr );

                // errors that are supposed to be displayed to user always come in json format
                if (jqXhr.hasOwnProperty('responseJSON')) {
                    var errorsObject = jqXhr.responseJSON;

                    var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

                    mylib.popupError(errorsHtml);
                }
                else {
                    var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

                    mylib.debugError('Error in main.js => loadListItems(): ' + errorThrown, debugData);
                }
            }
        });

    },


    showAjaxErrors: function(jqXhr, textStatus, errorThrown) {

        // errors that are supposed to be displayed to user always come in json format
        if (jqXhr.hasOwnProperty('responseJSON')) {
            var errorsObject = jqXhr.responseJSON;

            var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

            mylib.popupError(errorsHtml);
        }
        else {
            var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

            mylib.debugError('Error: ' + errorThrown, debugData);
        }
    },


    resetUploadInput: function(uploadInputName) {

        var inputId = '#mylib_' + uploadInputName + '_tmp_upload_value';
        var previewId = '#mylib_' + uploadInputName + '_tmp_upload_preview';
        var deleteLink = '#mylib_' + uploadInputName + '_delete_link';

        $(inputId).val('?reset');
        $(previewId).text('');
        $(deleteLink).hide();
    },



// This function works all ok and tested, but for now I just don't see any scenarious when
// I might need it. Maybe will be needed sometime in future, who knows.
//
//    /*
//        The format for inputValuePairs is like this:
//        var inputValuePairs = [
//            {'name':'John', 'value':'Doe'},
//            {'name':'Peter', 'value':'Pan'},
//        ];
//    */
//    postFormToUrl: function(url, inputValuePairs) {
//
//        var form = $('<form>', {
//            'method': 'POST',
//            'action': url
//        });
// 
//
//        for(key in inputValuePairs) { 
//            var hiddenInput = $('<input>', {
//                'name': inputValuePairs[key].name,
//                'type': 'hidden',
//                'value': inputValuePairs[key].value
//            });
//
//            form.append(hiddenInput);
//        }
//
//        form.appendTo('body');
//        form.submit();
//
//    },


};


// A fix for window.location.origin in Internet Explorer
// I need to have window.location.origin but this property isn't available in Internet Explorer.
// That is why I manually add it when necessary:
// http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/
if (!window.location.origin) {
    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
}

