/*

Usage example:

<a href="#" class="mylib-ajax-call" 

data-confirmation-message="Are you sure?"
data-ajax-call-url="{{ route('admin-blog-photo::sort-arrows') }}" 
data-method="post" 
data-on-success-function-name="mylib.refreshWindow"

>Link</a>



*/


mylib.ajaxCall = {

    initialize: function() {
//alert('init');
        this.methodLinks = $('.mylib-ajax-call');

        // ?? need to remove previously attached events. Will subsequent calls to initialize
        // attach multiple events?
        this.methodLinks.off('click');
        this.methodLinks.on('click', this.handleMethod);

        $(".mylib-ajax-call").addClass('mylib-ajax-call-already-inited');
        $(".mylib-ajax-call").removeClass('mylib-ajax-call');
    },

    handleMethod: function(e) {

        e.preventDefault();

        var obj = $(this);

        var confirmationMessage = obj.data('confirmation-message');
        if (confirmationMessage)
        {
            if (!mylib.confirm(String(confirmationMessage)))
            {
                return false;
            }
        }

        var commandData = obj.data(); // Calling .data() with no parameters retrieves all of the values as a JavaScript object

        // Pass here the current page URL, in certian scenarios it might be helpful
        commandData._callingPageUrl = window.location.href;

        var ajaxCallUrl = obj.data('ajax-call-url');

        var method = obj.data('method');

        var onSuccessFunctionName = obj.data('on-success-function-name');




        if (method === undefined) {
            method = 'post';
        }
        else {
            method = method.toLowerCase();
        }


        var jquerySelectorForDataInputs = obj.data('jquery-selector-for-data-inputs');

//alert(jquerySelectorForDataInputs);

        if (jquerySelectorForDataInputs !== undefined) {
            commandData.dataInputs = $( jquerySelectorForDataInputs ).serializeArray();

////console.log( 'here' );
////console.log( $( jquerySelectorForDataInputs ).serializeArray() );
        }

//console.log( $( this ).serializeArray() );



        var type;

        if (method == 'get') {
            type = 'GET';
        }
        else if (method == 'post') {
            type = 'POST';
            commandData._token = mylib.csrfToken();
        }
        else {
            type = 'POST';
            commandData._method = method;
            commandData._token = mylib.csrfToken();
        }


        $.ajax({
            type    : type,
            url     : ajaxCallUrl,
            data    : commandData,
            dataType: 'json',   // The type of data that you're expecting back from the server.
            success : function(data, textStatus, jqXhr) {

///alert('success');

                if (onSuccessFunctionName) 
                {
///alert('yey');
                    if (onSuccessFunctionName.substring(0, 6) == 'mylib.')
                    {
                        var tmpMyLibMethod = onSuccessFunctionName.substring(6);
                        mylib[tmpMyLibMethod](data, textStatus, jqXhr, commandData);
                    }
                    else
                    {
                        // commandData is the data that was passed in jquery request
                        window[onSuccessFunctionName](data, textStatus, jqXhr, commandData);
                    }
                }

                // I plan to implement mylib.handleAjaxActions that should be able to do redirect,
                // execute arbitrary javascripts and maybe close fancybox popup
                // see here how to execute javascript received from ajax:
                // http://stackoverflow.com/questions/978101/how-to-execute-javascript-inside-a-script-tag-returned-by-an-ajax-response
                // use $.globalEval(script);
                if (data.hasOwnProperty('redirectUrl') && (data.redirectUrl != '') )
                {
                    window.location.href = data.redirectUrl;
                }
            },
            error   : function(jqXhr, textStatus, errorThrown) {
                // errors that are supposed to be displayed to user always come in json format
                if (jqXhr.hasOwnProperty('responseJSON')) {
                    var errorsObject = jqXhr.responseJSON;

                    var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

                    mylib.popupError(errorsHtml);
                }
                else {
                    var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

                    mylib.debugError('Error in ajax-call.js => handleMethod: ' + errorThrown, debugData);
                }
            }
        }); // end of ajax call



        //
        //console.log( form.serializeArray() );
        //

    },

};
   




(function() {

    mylib.ajaxCall.initialize();

})();
