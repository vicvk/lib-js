/*

*/


mylib.returnBack = {

    initialize: function() {
///alert('init!');

        this.methodLinks = $('.mylib-return-back');
        // ?? need to remove previously attached events. Will subsequent calls to initialize
        // attach multiple events?
        this.methodLinks.off('click');
        this.methodLinks.on('click', this.handleMethod);
    },

    handleMethod: function(e) {
///alert('ops!');

        e.preventDefault();

        var link = $(this);
        var href = link.attr('href');

        var char;

        if ( href.indexOf('?') > -1 ) {
            char = '&';
        } else {
            char = '?';
        }

        var new_location;

        new_location = href + char + 'returnBack=' + mylib.fixedEncodeURIComponentWithSpaces(window.location.href);

        window.location.href = new_location;
    },

};






(function() {

    mylib.returnBack.initialize();

})();
