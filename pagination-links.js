
mylib.paginationLinks = 
{
    initialize: function() {
        this.methodLinks = $('ul.pagination li a');
        // ?? need to remove previously attached events. Will subsequent calls to initialize
        // attach multiple events?
        this.methodLinks.off('click');
        this.methodLinks.on('click', this.handleMethod);
    },
 
    handleMethod: function(e) {
        var link = $(this);

////            var paginatorLinksMode = link.data('paginator-links-mode');

        var href = link.attr('href');

////alert(href);

        if (href.substring(0, 1) == '#') {
            e.preventDefault();

            link.blur(); // remove focus from sort link

///alert('yes');



            href = href.substring(1); // remove the starting hash character
            var paginationQueryObj = mylib.parseQueryString(href);
///console.log(paginationQueryObj);

            var hashStr = window.location.hash.substring(1);
            var hashObj = mylib.parseQueryString(hashStr);
///console.log(hashObj);

            var newHashObj = mylib.mergeObjects(hashObj, paginationQueryObj);

//console.log(newHashObj);

            var newHashStr = mylib.buildSortedQueryString(newHashObj);

            // _pg=1 shouldn't appear in the hash string
            newHashStr = newHashStr.replace(/\&?\b_pg=1\b\&?/, '&');

            // remove the starting & character
            if (newHashStr.substring(0, 1) == '&') {
                newHashStr = newHashStr.substring(1);
            }

            // remove the ending & character
            if (newHashStr.substring(newHashStr.length-1) == '&') {
                newHashStr = newHashStr.substring(0, newHashStr.length - 1);
            }

            // all=undefined shouldn't appear in the hash string
            newHashStr = newHashStr.replace(/\&?\b_all=undefined\b\&?/, '&');

            // remove the starting & character
            if (newHashStr.substring(0, 1) == '&') {
                newHashStr = newHashStr.substring(1);
            }

            // remove the ending & character
            if (newHashStr.substring(newHashStr.length-1) == '&') {
                newHashStr = newHashStr.substring(0, newHashStr.length - 1);
            }


            if (newHashStr == '') {
                newHashStr = '#_all';
            }
            else {
                newHashStr = '#' + newHashStr;
            }

            var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search + newHashStr;

///alert(newUrl);

            window.location.replace(newUrl);

            mylib.loadListItems();
        }
    },
};


(function() {

    mylib.paginationLinks.initialize();

})();
