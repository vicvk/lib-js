/*

Usage example:

<form action="submit.php" method="post" class="mylib-ajax-form" 
data-confirmation-message="Are you sure?"
data-on-success-function-name="mylib.refreshWindow"
>


</form>


Useful links which I used for this solution:
http://stackoverflow.com/questions/5392344/sending-multipart-formdata-with-jquery-ajax
(post by Raphael Schweikert)

*/

(function() {
 
    var myVar = {
        initialize: function() {
//alert('init');
            this.methodLinks = $('.mylib-ajax-form');
            this.methodLinks.on('submit', this.handleMethod);
        },
     
        handleMethod: function(e) {
////alert('handle');
////return;
            e.preventDefault();

            var obj = $(this);

            var confirmationMessage = obj.data('confirmation-message');
            if (confirmationMessage)
            {
                if (!mylib.confirm(String(confirmationMessage)))
                {
                    return false;
                }
            }


            var ajaxCallUrl = obj.attr('action');

            var method = obj.attr('method');

            var onSuccessFunctionName = obj.data('on-success-function-name');


////alert(ajaxCallUrl);
////alert(method);

            if (method === undefined) {
                method = 'post';
            }
            else {
                method = method.toLowerCase();
            }

            var type;

            if (method == 'get') {
                type = 'GET';
            }
            else if (method == 'post') {
                type = 'POST';
            }
            else {
                type = 'POST';
            }


            // http://stackoverflow.com/questions/5392344/sending-multipart-formdata-with-jquery-ajax
            // (post by Raphael Schweikert)
            // For browsers that don't support FormData object, there is a fallback library
            // recommended on the link above.
            var commandData = new FormData(obj[0]);

            // !!!!!!!!!!!!!!!!!!!
            // !!!!!!!!!!!!!!!!!!!
            // NYI - detect if there are files being uploaded in the form and if yes,
            // display a moving gid indicating that the upload is in progress.
            // maybe even display a progress bar like described here:
            // 

            var opts = {

                type    : type,
                url     : ajaxCallUrl,
                data    : commandData,
                dataType: 'json',   // The type of data that you're expecting back from the server.
                cache   : false,
                contentType: false,
                processData: false,

                success : function(data, textStatus, jqXhr) {

///alert('success');


                    // Clear old errors
                    mylib.clearFormErrors(obj);


                    if (onSuccessFunctionName) 
                    {
///alert('yey');
                        if (onSuccessFunctionName.substring(0, 6) == 'mylib.')
                        {
                            var tmpMyLibMethod = onSuccessFunctionName.substring(6);
                            mylib[tmpMyLibMethod](data, textStatus, jqXhr, commandData);
                        }
                        else
                        {
                            window[onSuccessFunctionName](data, textStatus, jqXhr, commandData);
                        }
                    }

                    if (data.hasOwnProperty('redirectUrl') && (data.redirectUrl != '') )
                    {
                        window.location.href = data.redirectUrl;
                    }
                },
                error   : function(jqXhr, textStatus, errorThrown) {

///alert(jqXhr.status);

                   // Clear old errors
                    mylib.clearFormErrors(obj);

                    if (jqXhr.status == 422) {
                        var errorsObject = JSON.parse(jqXhr.responseText);

                        mylib.displayFormErrors(obj, errorsObject);

                    }
                    // errors that are supposed to be displayed to user always come in json format
                    else if (jqXhr.hasOwnProperty('responseJSON')) {
                        var errorsObject = jqXhr.responseJSON;

                        var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

                        mylib.popupError(errorsHtml);
                    }
                    else {
                        var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

                        mylib.debugError('Error in ajax-form.js => handleMethod: ' + errorThrown, debugData);
                    }
                }
            }; // end of opts


            if (commandData.fake)
            {
                // Make sure no text encoding stuff is done by xhr
                opts.xhr = function() { var xhr = jQuery.ajaxSettings.xhr(); xhr.send = xhr.sendAsBinary; return xhr; }
                opts.contentType = "multipart/form-data; boundary="+commandData.boundary;
                opts.data = commandData.toString();
            }

            $.ajax(opts);


            //
            //console.log( form.serializeArray() );
            //

        },
    };
   
    myVar.initialize();
 
})();


