/*
Send HTTP search request to form action URL.

<a href="#" class="mylib-search-command" data-jquery-selector-for-form=".mylib-search-form">Search</a>

class="mylib-search-command" is a required class attribute. Search links are selected by this class.

data-jquery-selector-for-form is not requred. Default value is ".mylib-search-form".

Minimal usage with all default values:
<a href="#" class="mylib-search-command">Search</a>

Minimal usage with all default values and styled as bootstrap button:
<a href="#" class="btn btn-info mylib-search-command" role="button"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search</a>

*/                                                                                                                                                                             

(function() {
 
    var myVar = {
        initialize: function() {
///alert('init');
            this.methodLinks = $('.mylib-search-command');
            // ?? need to remove previously attached events. Will subsequent calls to initialize
            // attach multiple events?
            this.methodLinks.off('click');
            this.methodLinks.on('click', this.handleMethod);

            this.methodForm = $('.mylib-search-form');
            // ?? need to remove previously attached events. Will subsequent calls to initialize
            // attach multiple events?
            this.methodForm.off('submit');
            this.methodForm.on('submit', this.handleMethod);
///alert('inited');
        },
     
        handleMethod: function(e) {
///alert('handle');

            e.preventDefault();

            var obj = $(this);

            if (obj.is('form')) {
///alert('form');
                var form = obj;
            }
            else {
///alert('link');
                var jquerySelectorForForm = obj.data('jquery-selector-for-form');

                if (jquerySelectorForForm === undefined)
                {
                    jquerySelectorForForm = '.mylib-search-form';
                }

                var form = $(jquerySelectorForForm);
            }

            var _search_route = form.find("input[name='_search_route']").val();
            var _base_search_url = form.find("input[name='_base_search_url']").val();
            var _base_search_url_mode = form.find("input[name='_base_search_url_mode']").val();

//alert(_search_route);
//alert(_base_search_url);

            if (_search_route) {
                //
                // if _search_route parameter is given, then we will make an ajax call to /search
                // url, asking for a clean search url for the given _search_route and parameters
                //

                var helperParams = mylib.getHelperParams();
                helperParams = jQuery.param(helperParams); // serialize to a query string

                var formData;
                if (helperParams == '') {
                    formData = form.serialize();
                }
                else {
                    formData = form.serialize() + '&' + helperParams;
                }

                var url_builder = window.location.protocol + '//' + window.location.host + '/search';

////alert(xurl);
////return;

                $.ajax({
                    type    : 'POST',
                    url     : url_builder,
                    data    : formData,
                    dataType: 'json',
                    success : function(data, textStatus, jqXhr) {

                        if (data.hasOwnProperty('redirectUrl') && (data.redirectUrl != '') )
                        {
                            window.location.href = data.redirectUrl;
                        }
                    },
                    error   : function(jqXhr, textStatus, errorThrown) {
                        // errors that are supposed to be displayed to user always come in json format
                        if (jqXhr.hasOwnProperty('responseJSON')) {
                            var errorsObject = jqXhr.responseJSON;

                            var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

                            mylib.popupError(errorsHtml);
                        }
                        else {
                            var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

                            mylib.debugError('Error in search-by-form.js => handleMethod: ' + errorThrown, debugData);
                        }
                    }
                }); // end of ajax call

            }
            else if (_base_search_url) {
////alert('123');

                //
                // if _base_search_url parameter is given, then we will construct a search url ourselves
                // inside this javascript. This is done by adding GET parameters to the given _base_search_url
                //
                var helperParams = mylib.getHelperParams();

////console.log(helperParams);

                var formData = mylib.parseQueryString( form.serialize() );

                var newQueryObj = mylib.mergeObjects(formData, helperParams);
                newQueryObj = mylib.selectNonEmptyProperties(newQueryObj);

                delete newQueryObj['_search_route'];
                delete newQueryObj['_base_search_url'];
                delete newQueryObj['_base_search_url_mode'];
                delete newQueryObj['_token'];

                var newQueryStr = mylib.buildSortedQueryString(newQueryObj);


                if (_base_search_url_mode == 'ajax') {
                    if (newQueryStr != '') {
                        newQueryStr = '#' + newQueryStr;
                    }

                    var newUrl = _base_search_url + newQueryStr;
                }
                else {
                    if (newQueryStr != '') {
                        newQueryStr = '?' + newQueryStr;
                    }

                    var newUrl = _base_search_url + newQueryStr + window.location.hash;
                }

                window.location.href = newUrl;

////alert(newUrl);

            }
            else {

///alert('here');

                //
                // if neither _search_route nor _base_search_url parameters are given, then we assume the search
                // should be performed via ajax and for that purpose we add search parameters
                // to the fragment(hash) part of the current URL.
                //

                var hashParams = mylib.parseQueryString(window.location.hash.substring(1));

                var helperParams = mylib.getHelperParams();

                var formData = mylib.parseQueryString( form.serialize() );

///                var newQueryObj = mylib.mergeObjects(hashParams, formData);
///                var newQueryObj = mylib.mergeObjects(newQueryObj, helperParams);

                // No need to take hashParams into account! The current formData
                // overwrites anything which is present in the current URL
                var newQueryObj = mylib.mergeObjects(formData, helperParams);

                newQueryObj = mylib.selectNonEmptyProperties(newQueryObj);

                delete newQueryObj['_search_route'];
                delete newQueryObj['_base_search_url'];
                delete newQueryObj['_base_search_url_mode'];
                delete newQueryObj['_token'];

                // NYI the pagination param may have a prefix, we don't handle it yet
                // because I haven't yet decided how to pass this prefix here. Of course we
                // can reset all params which names end in "_pg" but this may be undesired behaviour
                // when there are more then one list on the page, each having its own prefix.
                delete newQueryObj['_pg'];

                var newHashStr = mylib.buildSortedQueryString(newQueryObj);

                if (newHashStr == '') {
                    newHashStr = '#_all';
                }
                else {
                    newHashStr = '#' + newHashStr;
                }

                var newUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search + newHashStr;

                window.location.href = newUrl;

                mylib.loadListItems();
            }

            //
            //console.log( form.serializeArray() );
            //

        },
    };

    myVar.initialize();

})();


