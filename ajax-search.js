$(function() {
 
    var ajaxListContainers = $('table.mylib-ajax-list-container tbody, div.mylib-ajax-list-container, ul.mylib-ajax-list-container');

    if (ajaxListContainers.length > 0) {

        var searchForms = $('form.mylib-search-form');

        if (searchForms.length > 0) {

            // BEGIN - redirect to URL with # if there is ? 
            var currentUrl = window.location.href;
            var newUrl = currentUrl.replace(/\?/g , '#');

            if (currentUrl != newUrl) {

                window.location.href = newUrl;
                return; // exit from this function because we redirect anyway
            }
            // END - redirect to URL with # if there is ? 

            // BEGIN - init search form input values
            var params = mylib.getAllParams();

            searchForms.each(function( index ) {

                var searchForm = $( this );

                var searchFormInputs = searchForm.find(':input');

                searchFormInputs.each(function( index2 ) {

                    var searchFormInput = $( this );
                    var searchFormInputName = searchFormInput.attr('name');

                    if ((searchFormInputName != undefined) && (params.hasOwnProperty(searchFormInputName))) {

                        searchFormInput.val( params[searchFormInputName] );
                    }
                });
            });
            // END - init search form input values

        }

        // ------------------------------------------------------------------------------------

        mylib.loadListItems();
    }

});

