/*

Requires jQuery Form Plugin: http://malsup.com/jquery/form/
/js/jquery.form.min.js
Which should be included BEFORE ajax-form.js:
<script src="/js/jquery.form.min.js" type="text/javascript"></script>
<script src="/js/mylib/ajax-form.js"></script>



Usage example:

!!! rewrite: <form action="submit.php" method="post" class="mylib-ajax-form" 
!!! rewrite: data-confirmation-message="Are you sure?"
!!! rewrite: data-on-success-function-name="mylib.refreshWindow"
!!! rewrite: >
!!! rewrite: 
!!! rewrite: </form>




*/

(function() {
 
    var myVar = {
        initialize: function() {
//alert('init');

            var options = { 
                dataType: 'json',        // 'xml', 'script', or 'json' (expected server response type) 
                error: this.onError,
                success: this.onSuccess

                //target:        '#output1',   // target element(s) to be updated with server response 
                //beforeSubmit:  showRequest,  // pre-submit callback 

         
                // other available options: 
                //url:       url         // override for form's 'action' attribute 
                //type:      type        // 'get' or 'post', override for form's 'method' attribute 
                //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
                //clearForm: true        // clear all form fields after successful submit 
                //resetForm: true        // reset the form after successful submit 
         
                // $.ajax options can be used here too, for example: 
                //timeout:   3000 
            }; 

            $('.mylib-ajax-form').ajaxForm(options); 
        },

        onSuccess: function(data, textStatus, jqXhr, formObj) {
//alert('Success');

            // Clear old errors
            mylib.clearFormErrors(formObj);

            var onSuccessFunctionName = formObj.data('on-success-function-name');

            if (onSuccessFunctionName) 
            {
                if (onSuccessFunctionName.substring(0, 6) == 'mylib.')
                {
                    var tmpMyLibMethod = onSuccessFunctionName.substring(6);
                    mylib[tmpMyLibMethod](data, textStatus, jqXhr, formObj);
                }
                else
                {
                    window[onSuccessFunctionName](data, textStatus, jqXhr, formObj);
                }
            }

            // I plan to implement mylib.handleAjaxActions that should be able to do redirect,
            // execute arbitrary javascripts and maybe close fancybox popup
            // see here how to execute javascript received from ajax:
            // http://stackoverflow.com/questions/978101/how-to-execute-javascript-inside-a-script-tag-returned-by-an-ajax-response
            // use $.globalEval(script);
            if (data.hasOwnProperty('redirectUrl') && (data.redirectUrl != '') )
            {
                window.location.href = data.redirectUrl;
            }
        },

        onError: function(jqXhr, textStatus, errorThrown, formObj) {
//alert('Error');
///alert(jqXhr.status);

            // Clear old errors
            mylib.clearFormErrors(formObj);

            if (jqXhr.status == 422) {
                var errorsObject = JSON.parse(jqXhr.responseText);

                mylib.displayFormErrors(formObj, errorsObject);
            }
            // errors that are supposed to be displayed to user always come in json format
            else if (jqXhr.hasOwnProperty('responseJSON')) {
                var errorsObject = jqXhr.responseJSON;

                var errorsHtml = mylib.errorsObjectToHtmlList(errorsObject);

                mylib.popupError(errorsHtml);
            }
            else {
                var debugData = {'jqXhr':jqXhr, 'textStatus':textStatus, 'errorThrown':errorThrown};

                mylib.debugError('Error in ajax-form.js => handleMethod: ' + errorThrown, debugData);
            }
        },

    };
   
    myVar.initialize();
 
})();


